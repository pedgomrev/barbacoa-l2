== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Cañizares Romero, Ángel
* Domínguez Muñoz, Alejandro
* Garcia Moreno, Fermin
* Heredia Pérez, Elena
* Luque Giráldez, José Rafael
* Rodríguez Medina, José Francisco
* Rubio Carballo, Pablo

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Peugeot 508 (5 plazas)

* Alejandro Domínguez
* José Francisco
* Rafael Luque
* Fermin Garcia

==== Ford Tourneo (7 plazas)

* Ángel Cañizares
* Pablo Rubio
